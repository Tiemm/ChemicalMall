var Koa = require("koa")
const path = require("path")
const servicePath = path.dirname(__dirname) + "\\src\\service\\"
const utilPath = path.dirname(__dirname) + "\\src\\util\\"
const pageUtil = require(utilPath + "page")
const ProductService = require(servicePath + "Product")

var index = async (ctx, next) => {
    ctx.render("index.html")
    console.log("3")
}

var results = async (ctx, next) => {
    var req = ctx.request.body
    var param = ctx.query
    console.log(param)
    var keyword = req.keyword
    var userQuery = ctx.cookies.get("user-query")
    console.log(typeof userQuery)

    if (userQuery == null) {
        ctx.cookies.set("user-query", keyword)
    } else {
        console.log("搜索的关键词" + keyword)
        keyword = userQuery
    }

    var pageNo = param.pageNo
    var pageSize = param.pageSize
    console.log("pageNO:" + pageNo + "..类型是：" + typeof pageNo)
    console.log("pageSize" + pageSize + "..类型是：" + typeof pageSize)
    let page = {}
    var pageSort = await pageUtil.getPageSort(pageNo, pageSize)

    var exclude = ["description", "pictureStatus", "productStatus"]
    var dbPage = await ProductService.pageAndSorting(pageSort, keyword, exclude)

    //获取所有页数，查询总数
    page = await pageUtil.getPage(dbPage, pageSize)

    var products = dbPage.rows
    console.log("总共有几条：" + page.count + "..." + "总页数:" + page.totalPages)
    ctx.render("index.html", {products, page})
}


module.exports = {
    'GET /':index,
    "POST /search-result":results,
    "GET /search-result":results
}
