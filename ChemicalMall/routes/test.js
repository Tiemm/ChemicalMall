const path = require("path")
const servicePath = path.dirname(__dirname) + "\\src\\service\\"
const NoticeService = require(servicePath + "Notice")

var test = async (ctx, next) => {
    var content = "hahahahhaha"
    var notice = await NoticeService.addNotice(content)
    console.log(JSON.stringify(notice))
    ctx.render("index.html")
}

var testRemove = async (ctx, next) => {
    var notice = {
        id:1
    }
    var result = await NoticeService.remove(notice)
    console.log("View："+result)
    ctx.render("index.html")
}

module.exports = {
    "GET /test":test,
    "GET /testDel":testRemove
}
