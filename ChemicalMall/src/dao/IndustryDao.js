const path = require("path")
const modelPath = path.dirname(__dirname)
const model = require(modelPath + "\\util\\model")
var Industry = model.INDUSTRY

var IndustryDao = {
    async addIndustry (industry) {
        var addIndustry = await Industry.create(industry)

        return addIndustry
    },

    async findIndustry (industry) {
        var industryGet = await Industry.findById(industry.id)

        return industryGet
    },

    async listGrade () {
        var list = await Industry.findAll()

        return list
    },

    async updateIndustry (industry) {
        var oldIndustry = await findIndustry(industry)
        if (oldIndustry != null) {
            var affectedCount = await Industry.update(industry, {
                where: {
                    id: industry.id
                }
            })
            return affectedCount
        } else {
            var newIndustry = await addIndustry(industry)
            if (newIndustry != null) {
                return 1
            } else {
                return 0
            }
        }
    },

    async removeIndustry (industry) {
        var affectedCount = await Industry.destroy({
            where: {
                id: industry.id
            }
        })
        return affectedCount
    }
}

module.exports = IndustryDao