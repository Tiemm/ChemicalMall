const path = require("path")
const modelPath = path.dirname(__dirname)
const model = require(modelPath + "\\util\\model")
var Use = model.USE

var UseDao = {
    async addUse(use) {
        var newUse = await Use.create({
            content:use.content
        })

        return newUse
    },

    async findUse(use) {
        var oneUse = await Use.findById(use.id)

        return oneUse
    },

    async listUse() {
        var list = await Use.findAll()

        return list
    },

    async updateUse(use) {
        var oldUse = await this.findUse(use)
        if (oldUse != null){
            var affectedCount = await Use.update(use, {
                where:{
                    id:use.id
                }
            })
            return affectedCount
        } else {
            var newUse = await this.addUse(use)
            if (newUse != null) {
                return 1
            } else {
                return 0
            }
        }
    },

    async removeUse(use) {
        var affectedCount = await Use.destroy({
            where:{
                id:use.id
            }
        })
        return affectedCount
    }
}

module.exports = UseDao