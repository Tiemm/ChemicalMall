const path = require("path")
const modelPath = path.dirname(__dirname)
const model = require(modelPath + "\\util\\model")
const Sequelize = require(modelPath + "\\config\\sequelize")
var Product = model.PRODUCT

var ProductDao = {
    async addProduct(product) {
        var product = await Product.create(product)

        return product

    },

    async findProduct(product) {
        var id = product.id
        var pro = await Product.findById(id)

        return pro

    },

    async listProduct(page, key, exclude) {
        var likeValue = '%' + key + '%'
        var list = {}
        if (exclude == null) {
            //后台查询
            var pro = await Product.findAndCount({
                limit: page.limit, offset:page.offset,
                where: {
                    $or: [
                        {
                            name: {
                                $like: likeValue
                            }
                        },
                        {
                            localName: {
                                $like: likeValue
                            }
                        },
                        {
                            scientificName: {
                                $like: likeValue
                            }
                        },
                        {
                            englishName: {
                                $like: likeValue
                            }
                        },
                        {
                            shortName: {
                                $like: likeValue
                            }
                        }
                    ]
                }
            })

            return pro
        } else {
            //前台查询
            var pro = await Product.findAll({
                limit: page.limit, offset:page.offset, attributes: {
                    exclude: exclude,
                }, where: {
                    $or: [
                        {
                            name: {
                                $like: likeValue
                            }
                        },
                        {
                            localName: {
                                $like: likeValue
                            }
                        },
                        {
                            scientificName: {
                                $like: likeValue
                            }
                        },
                        {
                            englishName: {
                                $like: likeValue
                            }
                        },
                        {
                            shortName: {
                                $like: likeValue
                            }
                        }
                    ]
                }
            })

            return pro
        }
    },

    async removeProduct(product) {
        var affectedCount = await Product.destroy({
            where: {
                id: product.id
            }
        })
        return affectedCount
    },

    async findByStatus(product) {
        var sql = "SELECT * FROM tb_product WHERE picture_status = :pictureStatus OR product_status = :productStatus"
        var pro = await Sequelize.query(sql, {
            replacements: {
                pictureStatus: product.pictureStatus,
                productStatus: product.productStatus
            }
        })

        return pro

    },

    //分页查询
    async pageAndSorting(page, keyword, exclude) {
        var likeValue = '%' + keyword + '%'
        var list = {}
        console.log("limit = " + page.limit)
        console.log("offset = " + page.offset)
        if (exclude == null) {
            //后台查询

            var pro = await Product.findAndCount({
                limit: page.limit, offset:page.offset,
                where: {
                    $or: [
                        {
                            name: {
                                $like: likeValue
                            }
                        },
                        {
                            localName: {
                                $like: likeValue
                            }
                        },
                        {
                            scientificName: {
                                $like: likeValue
                            }
                        },
                        {
                            englishName: {
                                $like: likeValue
                            }
                        },
                        {
                            shortName: {
                                $like: likeValue
                            }
                        }
                    ]
                }
            })

            return pro
        } else {
            //前台查询
            var pro = await Product.findAndCount({
                limit: page.limit, offset:page.offset, attributes: {
                    exclude: exclude,
                }, where: {
                    $or: [
                        {
                            name: {
                                $like: likeValue
                            }
                        },
                        {
                            localName: {
                                $like: likeValue
                            }
                        },
                        {
                            scientificName: {
                                $like: likeValue
                            }
                        },
                        {
                            englishName: {
                                $like: likeValue
                            }
                        },
                        {
                            shortName: {
                                $like: likeValue
                            }
                        }
                    ]
                }
            })

            return pro
        }
    }
}

module.exports = ProductDao

