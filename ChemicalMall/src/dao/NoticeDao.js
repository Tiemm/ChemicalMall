const path = require("path")
const modelPath = path.dirname(__dirname)
const model = require(modelPath + "\\util\\model")
const mapping = require(modelPath + "\\config\\mapping")

var Notice = model.NOTICE


var NoticeDao = {
    async addNotice(content) {
        var notice = await Notice.create({notice: content});
        console.log("dao层：" + JSON.stringify(notice))
        return notice
    },

    async removeNotice(notice) {
        var result = await Notice.destroy({
            where: {
                id: notice.id
            }
        })
        console.log("Dao:"+result)
        return result
    },

    async findNotice (notice) {
        var id = notice.id
        var result = await Notice.findById(id)
        console.log(JSON.stringify(result))
        return result

    },

    async listNotice () {
        var listNotice = await Notice.findAll()
        return listNotice
    },

    async updateNotice (notice) {
        var oldNotice = await findNotice(notice)
        if (oldNotice != null) {
            var result = Notice.update(notice, {
                where: {
                    id: notice.id
                }
            })
            return result
        } else {
            var newNotice = await addNotice(notice)
            if (newNotice != null) {
                return 1
            } else {
                return 0
            }
        }
    }

}

module.exports = NoticeDao