const path = require("path")
const modelPath = path.dirname(__dirname)
const Sequelize = require(modelPath + "\\config\\sequelize")
const model = require(modelPath + "\\util\\model")
const mapping = require(modelPath + "\\config\\mapping")
var Category = model.CATEGORY
var Product = model.PRODUCT


var CategoryDao = {
    async addCategory (category) {
        var hasParent = await this.getByName(category)
        if (hasParent == [] && hasParent.parentId == null) {
            var hasParentCate = await Category.create({
                name: category.name
            })

            return hasParentCate
        } else {
            var noParentCate = await Category.create({
                name: category.name,
                parentId: category.parentId
            })
            return noParentCate
        }
    },

    async getByName (category) {
        var sql = "SELECT * FROM tb_category WHERE name = :name"
        var byNameCate = await Sequelize.query(sql, {
            replacement: {
                name: category.name
            }, type: Sequelize.QueryTypes.SELECT
        })

        return byNameCate
    },

    async removeCategory (category) {
        var affectedCount = await Category.destroy({
            where: {
                id: category.id
            }
        })
        return affectedCount
    },

    async findCategory (category) {

        var one = await Category.findById(category.id)

        return one
    },

    async updateCategory (category) {
        var oldCategory = await findCategory(category)
        if (oldCategory != null) {
            var affectedCount = await Category.update(category, {
                where: {
                    id: category.id
                }
            })
            return affectedCount
        } else {
            var newCategory = await addCategory(category)
            return 1
        }
    },

    async listCategory (limit) {
        var list = await Category.findAll({limit:limit})

        return list
    },

}

module.exports = CategoryDao

