const model = require(modelPath + "\\util\\model")
const mapping = require(modelPath + "\\config\\mapping")
const Sequelize = require(modelPath + "\\config\\sequelize")
var Grade = model.GRADE


var GradeDao = {
    async addGrade (grade) {
        var hasParent = await this.getByName(grade)
        if (hasParent == [] && hasParent.parentId == null) {
            var noParentGrade = await Grade.create({
                name: grade.name
            })

            return noParentGrade
        } else {
            var hasParentGrade = await Grade.create({
                name: grade.name,
                parentId: grade.parentId
            })

            return hasParentGrade
        }
    },

    async findGrade (grade) {
        var gradeClass = await Grade.findById(grade.id)

        return gradeClass
    },

    async listGrade() {
        var list = await Grade.findAll()

        return list
    },

    async updateGrade (grade) {
    var oldGrade = await this.findGrade(grade)
        if (oldGrade != null) {
            var affectedCount = await Grade.update(grade, {
                where: {
                    id: grade.id
                }
            })
            return affectedCount
        } else {
            var newGrade =  await this.addCategory(grade)
            return 1
        }
    },

    async removeGrade (grade) {
        var affectedCount = await Grade.destroy({
            where:{
                id:grade.id
            }
        })
        return affectedCount
    },

    async findByName (grade) {
        var sql = "SELECT * FROM tb_grade WHERE name = :name"
        var nameGrade = await Sequelize.query(sql, {replacement:{
            name:grade.name
        }, type:Sequelize.QueryTypes.SELECT})

        return nameGrade
    }

}

module.exports = GradeDao