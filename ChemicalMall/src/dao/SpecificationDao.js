const path = require("path")
const modelPath = path.dirname(__dirname)
const model = require(modelPath + "\\util\\model")
const Sequelize = require(modelPath + "\\config\\sequelize")
var Specification = model.SPECIFICATION


var SpecificationDao = {
    async addSpecification (specification) {
        var spe = await Specification.create(specification)

        return spe
    },

    async findSpecification (specification) {
        var spe = await Specification.findById(specification.id)

        return spe
    },

    async listSpecification () {
        var list = await Specification.findAll()

        return list
    },

    async updateSpecification (specification) {
        var oldSpecification = await this.findSpecification(specification)
        if (oldSpecification != null) {
            var affectedCount = await Specification.update(specification, {where:{
                id:specification.id
            }})
            return affectedCount
        } else {
            var newSpecification = await this.addSpecification(specification)
            if (newSpecification != null) {
                return 1
            } else {
                return 0
            }
        }
    },

    async removeSpecification (specification) {
        var affectedCount = await Specification.destroy({where:{id:specification.id}})
        return affectedCount
    },

    async findByProperty (productId, specification) {
        var sql = "SELECT * FROM tb_specification WHERE property = :property AND product_id = :productId"
        var spe = await Sequelize.query(sql, {replacement:{
            property:specification.property,
            productId:productId
        }, type:Sequelize.QueryTypes.SELECT})

        return spe
    }
}

module.exports = SpecificationDao