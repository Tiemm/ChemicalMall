const pageSort = {
    async getPageSort(pageNo, pageSize) {
        var limit = pageNo * pageSize
        var offset = limit - pageSize
        var pageSort = {
            limit:limit,
            offset:offset
        }
        return pageSort
    },

    async getPage(dbPage, pageSize) {
        var totalPages
        var count = dbPage.count

        if (count <= pageSize) {
            totalPages = 1
        } else {
            var result = count/pageSize
            totalPages = result
        }
        var page = {
            totalPages:totalPages,
            count:count
        }
        return page

    }
}

module.exports = pageSort

