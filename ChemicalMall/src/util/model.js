const fs = require("fs")
const path = require("path")
const modelPath = path.dirname(__dirname) + "\\model\\"
const sequelizePath = path.dirname(__dirname)
const sequeslize = require(sequelizePath + "\\config\\sequelize")

let files = fs.readdirSync(modelPath)

let js_files = files.filter((f) => {
    return f.endsWith(".js")
}, files)


module.exports = {}

for (let f of js_files) {
    console.log("import model from /model")
    let name = f.substring(0, f.length - 3).toUpperCase()
    console.log(name)
    module.exports[name] = sequeslize.import(modelPath + f)
}