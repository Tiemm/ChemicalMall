var now = Date.now()
module.exports = function (sequelize, DataTypes) {
    return sequelize.define("specification", {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true,
            unique:true,
            comment:"逻辑主键"
        },
        property:{
            type:DataTypes.STRING,
            allowNull:false,
            comment:"规格的属性名称"
        },
        value:{
            type:DataTypes.STRING,
            allowNull:false,
            defaultValue:"",
            comment:"规格属性名称对应的值"
        },
        productId:{
            type:DataTypes.INTEGER,
            references:{
                model:"product",
                key:"id"
            },
            field:"product_id"
        }
    },{
        timestamp:true,
        underscored:true,
        tableName:"tb_specification",
        charset:"utf8",
        comment:"规格表"
    })
}