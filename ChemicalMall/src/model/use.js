module.exports = function (sequelize, DataTypes) {
    return sequelize.define("use", {
        id:{
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true,
            comment:"主键"
        },
        productId:{
            type:DataTypes.INTEGER,
            field:"product_id",
            unique:true,
            reference:{
                module:'product',
                key:'id'
            },
            comment:"外键-参考product的"
        },
        content:{
            type:DataTypes.TEXT,
            allowNull:false,
            comment:"用途说明"
        }

    }, {
        timestamp:true,
        underscored:true,
        tableName:"tb_use",
        charset:"utf8",
        comment:"用途表"
    })
}