module.exports = function (sequelize, DataTypes) {
    return sequelize.define("notice", {
        id:{
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        notice:{
            type:DataTypes.TEXT,
            allowNull:false,
            comment:"注意事项的内容"
        }
    },{
        timestamp:true,
        underscored:true,
        tableName:"tb_notice",
        comment:"注意事项表"
    })
}