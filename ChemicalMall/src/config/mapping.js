const sequelize = require("./sequelize")
const path = require("path")


var modelPath = path.dirname(__dirname)

var product = sequelize.import(modelPath +"\\model\\product.js")
var notice = sequelize.import(modelPath +"\\model\\notice.js")
var use = sequelize.import(modelPath +"\\model\\use.js")
var specification = sequelize.import(modelPath +"\\model\\specification.js")
var industry = sequelize.import(modelPath +"\\model\\industry.js")
var category = sequelize.import(modelPath +"\\model\\category.js")
var grade = sequelize.import(modelPath + "\\model\\grade.js")

product.hasOne(use)//一对一
product.hasOne(notice)//一对一
product.hasMany(specification)//一个原模型可以连接多个目标模型,一对多
//specification.belongsToMany(product, {through:"tb_product_specification", as:"ProductSpecification"})
use.hasOne(industry)//一对一
category.hasOne(category, {foreignKey:"parent_id", targetKey:"id"})
grade.hasOne(grade, {foreignKey:"parent_id", targetKey:"id"})
//多对多
product.belongsToMany(category,{through:"tb_product_category", as:"ProductCategory"})
category.belongsToMany(product,{through:"tb_product_category", as:"ProductCategory"})
product.belongsToMany(grade, {through:"tb_product_grade", as:"ProductGrade"})



sequelize.sync()
