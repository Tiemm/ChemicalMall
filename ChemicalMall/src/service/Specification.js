const path = require("path")
const modelPath = path.dirname(__dirname)
const SpeDao = require(modelPath + "\\dao\\SpecificationDao")
const model = require(modelPath + "\\util\\model")

const SpecificationService = {
    async add (specification) {
        var spe = await SpeDao.addSpecification(specification)
        var speJSON = JSON.stringify(spe)
        return speJSON
    },

    async findOne (specification) {
        var spe = await SpeDao.findSpecification(specification)
        var speJSON = JSON.stringify(spe)

        return speJSON
    },

    async list () {
        var list = await SpeDao.listSpecification()
        var listJSON = JSON.stringify(list)

        return listJSON
    },

    async update (specification) {

            var affectedCount = await SpeDao.updateSpecification(specification)
            return affectedCount

    },

    async remove (specification) {
        var affectedCount = await SpeDao.removeSpecification(specification)
        return affectedCount
    },

    async findByProperty (productId, specification) {

        var spe = await SpeDao.findByProperty(productId, specification)
        var speJSON = JSON.stringify(spe)
        return speJSON
    }
}

module.exports = SpecificationService