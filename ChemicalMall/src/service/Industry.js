const path = require("path")
const modelPath = path.dirname(__dirname)
const IndustryDao = require(modelPath + "\\dao\\IndustryDao")
const model = require(modelPath + "\\util\\model")

const IndustryService = {
    async add (industry) {
        var add = await IndustryDao.addIndustry(industry)
        var addJSON = JSON.stringify(add)
        return addJSON
    },

    async findOne (industry) {
        var industryGet = await IndustryDao.findIndustry(industry)
        var oneJSON = JSON.stringify(industryGet)

        return oneJSON
    },

    async list () {
        var list = await IndustryDao.listGrade()
        var listJSON = JSON.stringify(list)
        return listJSON
    },

    async update (industry) {
        var affectedCount = await IndustryDao.updateIndustry(industry)
        return affectedCount
    },

    async remove (industry) {
        var affectedCount = await IndustryDao.removeIndustry(industry)
        return affectedCount
    }
}

module.exports = IndustryService