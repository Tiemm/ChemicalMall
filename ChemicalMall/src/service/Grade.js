const path = require("path")
const modelPath = path.dirname(__dirname)
const GradeDao = require(modelPath + "\\dao\\GradeDao")
const model = require(modelPath + "\\util\\model")

const GradeService = {
    async add(grade) {
        var newGrade = await GradeDao.addGrade(grade)
        var newGradeJSON = JSON.stringify(newGrade)
        return newGradeJSON
    },

    async getByName (grade) {
        var gradeByName = await GradeDao.getByName(grade)
        var gradeByNameJSON = JSON.stringify(gradeByName)
        return gradeByNameJSON
    },

    async remove (grade) {
        var affectedCount = await GradeDao.removeGrade(grade)
        return affectedCount
    },

    async findOne (grade) {

        var one = await GradeDao.findGrade(grade)
        var oneJSON = JSON.stringify(one)

        return oneJSON
    },

    async update (grade) {
        var affectedCount = await GradeDao.updateGrade(grade)
        return affectedCount

    },

    async listGrade (limit) {
        var list = await GradeDao.listGrade(limit)
        var listJSON = JSON.stringify(list)

        return listJSON
    },

}

module.exports =GradeService