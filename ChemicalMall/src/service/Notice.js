const path = require("path")
const modelPath = path.dirname(__dirname)
const NoticeDao = require(modelPath + "\\dao\\NoticeDao")
const model = require(modelPath + "\\util\\model")

const NoticeService = {
    async addNotice(content) {
        var notice = await NoticeDao.addNotice(content)
        var noticeJSON = JSON.stringify(notice)
        return noticeJSON
    },

    async remove(notice){
        var result = await NoticeDao.removeNotice(notice)
        console.log("Service:"+result)
        return result
    },

    async findOne(notice) {
        var result = await NoticeDao.findNotice(notice)
        var resultJSON = JSON.stringify(result)
        return resultJSON
    },

    async getAll() {
        var list = await NoticeDao.listNotice()
        var listJSON = JSON.stringify(list)
        return listJSON
    },

    async update(notice) {
        var affectedCount = await NoticeDao.updateNotice(notice)
        return affectedCount
    }
}

module.exports = NoticeService


