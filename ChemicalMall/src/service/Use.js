const path = require("path")
const modelPath = path.dirname(__dirname)
const UseDao = require(modelPath + "\\dao\\UseDao")
const model = require(modelPath + "\\util\\model")

const UseService = {
    async add(use) {
        var newUse = await UseDao.addUse(use)
        var newUseJSON = JSON.stringify(newUse)
        return newUseJSON
    },

    async update(use) {
        var affectedCount = await UseDao.updateUse(use)
        return affectedCount
    },

    async remove(use) {
        var affectedCount = await UseDao.removeUse(use)
        return affectedCount
    },

    async list() {
        var list = await UseDao.listUse()
        var listJSON = JSON.stringify(list)
        return listJSON
    }
}

module.exports = UseService