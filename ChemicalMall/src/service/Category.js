const path = require("path")
const modelPath = path.dirname(__dirname)
const CategoryDao = require(modelPath + "\\dao\\CategoryDao")
const model = require(modelPath + "\\util\\model")

const CategoryService = {
    async add(category) {
        var newCate = await CategoryDao.addCategory(category)
        var newCateJSON = JSON.stringify(newCate)
        return newCateJSON
    },

    async getByName (category) {
        var cateByName = await CategoryDao.getByName(category)
        var cateByNameJSON = JSON.stringify(cateByName)
        return cateByNameJSON
    },

    async remove (category) {
        var affectedCount = await CategoryDao.removeCategory(category)
        return affectedCount
    },

    async findOne (category) {

        var one = await CategoryDao.findCategory(category)
        var oneJSON = JSON.stringify(one)

        return oneJSON
    },

    async update (category) {
        var affectedCount = await CategoryDao.updateCategory(category)
        return affectedCount

    },

    async listCategory (limit) {
        var list = await CategoryDao.listCategory(limit)
        var listJSON = JSON.stringify(list)

        return listJSON
    },

}

module.exports = CategoryService