const path = require("path")
const modelPath = path.dirname(__dirname)
const ProductDao = require(modelPath + "\\dao\\ProductDao")
const model = require(modelPath + "\\util\\model")

const ProductService = {
    async add(product) {
        return await ProductDao.addProduct(product, use, industry)
    },

    async findProduct (product) {
        return await ProductDao.findProduct(product)
    },

    async listProduct (page, key, exclude) {

        return await ProductDao.listProduct(page, key, exclude)

    },

    async remove (product) {
        return await ProductDao.removeProduct(product)
    },

    async findByStatus (product) {
        return await ProductDao.findByStatus(product)
    },

    async pageAndSorting (page, keyword, exclude) {
        var pageable = await ProductDao.pageAndSorting(page, keyword, exclude)
        return pageable
    }
}

module.exports= ProductService

