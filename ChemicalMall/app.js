let templating = require("./templating")
const controller = require("./controller")
const Koa = require("koa")
const serve = require("koa-static")
const Router = require("koa-router")
const views = require("koa-views")
const render = require("koa-views-render")
const router = new Router()
const bodyParser = require("koa-bodyparser")
var app = new Koa();
const isProduction = process.env.NODE_ENV === 'production';
console.log("isProduction:" + isProduction)

app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    var
        start = new Date().getTime(),
        execTime;
    await next();
    execTime = new Date().getTime() - start;
    ctx.response.set('X-Response-Time', `${execTime}ms`);
});
//koa-views
//app.use(views(__dirname + '\\views\\', {map:{html:"nunjucks"}}))

//koa-static
app.use(serve(__dirname + '\\public\\'))

/*if (! isProduction) {
    let staticFiles = require('./static_files');
    app.use(staticFiles('/public/', __dirname + '/public'));
}*/

//koa-bodyParser
app.use(bodyParser());

app.use(templating('views', {
    noCache: !isProduction,
    watch: !isProduction
}));

//koa-router
app.use(controller())



app.listen(3000);
